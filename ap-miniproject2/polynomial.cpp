/*
	Emil Stensig Jensen
	esje11@student.aau.dk
	studynumber: 2011 2745
	*/

#include <vector>
#include <cmath>
#include <algorithm>
#include <future>
#include "polynomial.h"

namespace mathpolynomial {
	template<typename T>

	// 1.a: Default constructor to create a trivial polynomial: 0.
	polynomial<T>::polynomial()
		: poly{}
	{
		poly.push_back(0);
	}

	// 5. Member functions accepting containers should support any type of container, including array types (Item 13); also support braced initializers (Item 7).
	template<typename T>
	template<typename ITER>
	polynomial<T>::polynomial(ITER begin, ITER end)
		: poly{}
	{
		while (begin != end)
		{
			poly.push_back(*begin);
			begin++;
		}
	}

	// 1.b: Constructor for specific degree term coefficients.
	// 5. Member functions accepting containers should support any type of container, including array types (Item 13); also support braced initializers (Item 7).
	template<typename T>
	polynomial<T>::polynomial(std::initializer_list<T> lst)
		: poly{}
	{
		// 3. Use auto where applicable (Item 5). It is enough to mention just once in comments that you are following this requirement, but it should be enforced throughout the source code.
		auto begin = lst.begin();
		poly.push_back(*begin);
		begin++;

		while (begin != lst.end())
		{
			poly.push_back(*begin);
			begin++;
		}
	}

	template<typename T>
	polynomial<T>::polynomial(const polynomial<T>& p)
		: polynomial{ p.poly.cbegin(), p.poly.cend() }
	{
	}
	
	// 7. Implement move semantics (Chapter 5) by using either Pimpl idiom (Items 22,17,8) or smart pointers (Item 17, Chapter 4).
	// I have _not_ used Pimpl or smart pointers for this.
	template<typename T>
	polynomial<T>::polynomial(polynomial<T>&& p) {
		poly = std::move(p.poly);
		integration_cache = std::move(p.integration_cache);
	}

	// 1.c: A method to scale the polynomial, i.e. multiply by a scalar value.
	template<typename T>
	void polynomial<T>::scale(int s) {
		// The polynomial is changed, so lock it and clear the cache.
		std::lock_guard<std::mutex> guard(m);
		integration_cache.clear();

		for (T& p : poly) {
			p *= s;
		}
	}

	// 1.d: A method to add a root r, i.e. multiply by a term (x-r).
	template<typename T>
	void polynomial<T>::add_term(int power, T coefficient) {
		// The polynomial is changed, so lock it and clear the cache.
		std::lock_guard<std::mutex> guard(m);
		integration_cache.clear();

		// Fill with 0-coefficients until we reach the right power.
		while (poly.size() <= power)
		{
			poly.push_back(0);
		}

		poly.at(power) += coefficient;
	}

	// 1.e: A method to add several roots at once.
	// The indexes in each parameter should correspond to each other.
	template<typename T>
	void polynomial<T>::add_terms(std::vector<T> powers, std::vector<T> coefficients) {
		if (powers.size() != coefficients.size()) {
			throw std::length_error("powers and coefficients did not have same length.");
		}
		else
		{
			for (size_t i = 0; i < powers.size(); i++)
			{
				this->add_term(powers.at(i), coefficients.at(i));
			}
		}
	}

	// 1.f: A method to valuate the polynomial at a given point.
	template<typename T>
	T polynomial<T>::valuate(T xval) const {
		T result{};

		for (size_t i = 0; i < poly.size(); i++)
		{
			result += poly.at(i) * pow(xval, i);
		}

		return result;
	}

	// 1.g: A method to compute a polynomial which is a derivative of the polynomial.
	template<typename T>
	polynomial<T> polynomial<T>::derive() const {
		// Trivial cases
		if (poly.size() < 2) {
			return polynomial < T > {0};
		}
		else
		{
			std::vector<T> vec{ ++(poly.cbegin()), poly.cend() };
			for (size_t i = 1; i < vec.size(); i++)
			{
				vec.at(i) *= (i + 1);
			}
			return polynomial < T > {vec.cbegin(), vec.cend()};
		}
	}

	template<typename T>
	T polynomial<T>::integrate_helper(T a, T b, std::true_type) const {
		static_assert(false, "cannot integrate using integral types");
	}

	template<typename T>
	T polynomial<T>::integrate_helper(T a, T b, std::false_type) const {
		// 9. Use lambda expressions (Chapter 6, e.g. when computing sums during evaluation of a polynomial; dispatch asynchronous computation).
		auto integrate_parts = [=](T val) {
			if (integration_cache.count(val) > 0) {
				return integration_cache.at(val);
			}
			else
			{
				T res{};
				for (size_t i = 0; i < poly.size(); i++)
				{
					res += (poly.at(i) / (i + 1)) * pow(val, i + 1);
				}

				std::lock_guard<std::mutex> guard(m);
				// 6. Cache the integral data to avoid repetitive integration (Items 16, 40). Make it thread-safe.
				integration_cache.emplace(val, res);

				return res;
			}

		};

		// 10. Use concurrency (Chapter 7, e.g. compute products of polynomials by evaluating each polynomial asynchronously and then multiplying the final product).
		auto a_part = std::async(integrate_parts, a);
		auto b_part = std::async(integrate_parts, b);

		return  b_part.get() - a_part.get();
	}

	// 1.h: A method to compute an integral for given interval bounds.
	template<typename T>
	T polynomial<T>::integrate(const T a, const T b) const {
		// 8. Use type traits (see examples in Item 27, e.g. disable or fail assertion for the integration method over integer types).
		typename std::is_integral<T>::type is_int;
		return integrate_helper(a, b, is_int);
	}

	template<typename T>
	int polynomial<T>::degree() const {
		return poly.size() - 1;
	}

	template<typename T>
	T polynomial<T>::at(std::size_t index) const {
		return poly.at(index);
	}

	template<typename T>
	std::ostream& operator<<(std::ostream& os, const polynomial<T>& p) {
		for (size_t i = p.degree() - 1; i > 0; i--)
		{
			if (p.at(i) != 0) {
				os << p.at(i) << "x^" << i << "+";
			}
		}
		os << p.at(0) << endl;
		return os;
	}
}