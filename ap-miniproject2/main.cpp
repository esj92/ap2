/*
Emil Stensig Jensen
esje11@student.aau.dk
studynumber: 2011 2745
*/

#include <iostream>
#include <vector>
#include "polynomial.h"
#include "polynomial.cpp"
#include <stdexcept>

using namespace std;
using namespace mathpolynomial;

void copy_ctor() {
	polynomial<int> p{ 1, 2, 3 };
	polynomial<int> p2{ p };

	if (p != p2){
		cout << "copy constructor error." << endl;
	}
}

void degrees() {
	polynomial<int> p{};
	if (p.degree() != 0) {
		cout << "Wrong degree" << endl;
	}


	polynomial<int> p5{ 1, 2, 3, 4, 5 };
	if (p5.degree() != 4) {
		cout << "Wrong degree (expected 4)" << endl;
	}
}

void req_1a() {
	polynomial<int> p{};
	polynomial<int> p2{};
	if (p != p2) {
		cout << "Equality error" << endl;
	}
}

void req_1b() {
	polynomial<int> p{ 1, 2, 3, 4, 5 };
	for (size_t i = 1; i < 5; i++)
	{
		if (p.at(i - 1) != i) {
			cout << "coefficient error" << endl;
		}
	}

	vector<int> vec{ 1, 2, 3, 4, 5 };
	polynomial<int> p2{ vec.cbegin(), vec.cend() };
	for (size_t i = 1; i < 5; i++)
	{
		if (p2.at(i - 1) != i) {
			cout << "coefficient error" << endl;
		}
	}

	if (p != p2) {
		cout << "Equality error" << endl;
	}
}

void req_1c() {
	polynomial<int> p{ 1, 2, 3, 4, 5 };
	p.scale(5);
	for (size_t i = 1; i <= p.degree(); i++)
	{
		if (p.at(i - 1) != i * 5) {
			cout << "Scaling error" << endl;
		}
	}
}

void req_1d() {
	polynomial<int> p{};
	p.add_term(5, 2);
	for (size_t i = 0; i < p.degree() - 1; i++)
	{
		if (p.at(i) != 0) {
			cout << "1.d error" << endl;
		}
	}
	if (p.at(5) != 2) {
		cout << "1.d error 2" << endl;
	}
}

void req_1e() {
	polynomial<int> p{};
	vector<int> powers{ 1, 2, 3 };
	vector<int> coefficients{ 5, 6, 7 };

	p.add_terms(powers, coefficients);
	if (p.at(0) != 0 || p.at(1) != 5 || p.at(2) != 6 || p.at(3) != 7) {
		cout << "error with add_terms" << endl;
	}
}

void req_1f() {
	polynomial<int> p{ 1, 2, 6, 1, 7 };
	if (p.valuate(15) != 359131) {
		cout << "1f error" << endl;
	}

	if (p.valuate(1) != 17) {
		cout << "1f error2" << endl;
	}

	if (p.valuate(0) != 1) {
		cout << "1f error3" << endl;
	}
}

void req_1g() {
	polynomial<int> p{ 5, 4, 1, 6 };
	auto d = p.derive();
	if (d.at(0) != 4 || d.at(1) != 2 || d.at(2) != 18) {
		cout << "1g error" << endl;
	}

	auto d2 = d.derive();
	if (d2.at(0) != 2 || d2.at(1) != 36) {
		cout << "1g error2" << endl;
	}

	auto d3 = d2.derive();
	if (d3.at(0) != 36) {
		cout << "1g error3" << endl;
	}

	auto d4 = d3.derive();
	if (d4.at(0) != 0) {
		cout << "1g error4" << endl;
	}
}

void req_1h() {
	polynomial<double> p{ 1, 2, 3 };
	double integrated{ p.integrate(1, 2) };
	if (integrated != 11) {
		cout << "1h error" << endl;
	}
}

void req_1i() {
	polynomial<int> p{ 1, 2, 3 };
	polynomial<int> p2{ 3, 4, 5 };
	auto p3 = p + p2;
	if (p3.at(0) != 4 || p3.at(1) != 6 || p3.at(2) != 8) {
		cout << "1i error" << endl;
	}
}

void req_1j() {
	polynomial<int> p{ 1, 2, 3 };
	polynomial<int> p2{ 3, 4, 5, 6, 7 };
	auto p3 = p * p2;

	if (p3.at(0) != 3 || p3.at(1) != 10 || p3.at(2) != 22 || p3.at(3) != 28 || p3.at(4) != 34 || p3.at(5) != 32 || p3.at(6) != 21) {
		cout << "1j error" << endl;
	}
}

void req_2() {
	polynomial<int> p1{ 1, 2, 3, 4 };
	polynomial<double> p2{ 1, 2, 3, 4 };
}

void req_7() {
	polynomial<double> p{ 1, 2, 3 };

	p.integrate(1, 2);
	p.integrate(3, 4);

	auto p2(std::move(p));

	//cout << p << endl; // This polynomial is no longer valid.
	cout << p2 << endl;
}

void req_8() {
	polynomial<int> p2{ 1, 2, 3 };
	//p2.integrate(1, 2); // disabled because it works
}

void do_all_tests() {
	copy_ctor();
	degrees();
	req_1a();
	req_1b();
	req_1c();
	req_1d();
	req_1e();
	req_1f();
	req_1g();
	req_1h();
	req_1i();
	req_1j();
	req_2();
	req_7();
	req_8();
}

int main() {
	do_all_tests();

	cout << "done";
	cin.get();
	return 0;
}