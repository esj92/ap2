/*
Emil Stensig Jensen
esje11@student.aau.dk
studynumber: 2011 2745
*/

#ifndef __POLY__
#define __POLY__

#include <algorithm>
#include <mutex>
#include <map>

namespace mathpolynomial {
	// 2. Make a template class of Polynomial for a given type of coefficients (e.g. floating point types, complex numbers; also support integer types in all operations except integration).
	template<typename T>
	class polynomial{
		// index = exponent, value = coefficient
		std::vector<T> poly;

		// Used when integrating
		T integrate_helper(T a, T b, std::true_type) const; // Cannot integrate over int
		T integrate_helper(T a, T b, std::false_type) const; // Can integrate over other types

		// Thread safety when caching
		mutable std::mutex m;
		// Cache
		mutable std::map<T, T> integration_cache;

	public:
		polynomial();

		template<typename ITER>
		polynomial(ITER begin, ITER end);

		polynomial(std::initializer_list<T>);

		polynomial(const polynomial<T>&);

		polynomial(polynomial<T>&&);

		void scale(int);

		void add_term(int power, T coefficient);

		void add_terms(std::vector<T> powers, std::vector<T> coefficients);

		// 4. Use const where applicable (e.g. function arguments and methods, see also Item 13). It is enough to mention just once in comments that you are following this requirement, but it should be enforced throughout the source code.
		T valuate(T) const;

		polynomial<T> derive() const;

		T integrate(const T a, const T b) const;

		// 1.i: A plus operator to return a polynomial equal to a sum of two polynomials.
		friend polynomial operator+(const polynomial<T>& p1, const polynomial<T>& p2) {			
			polynomial<T> poly{ p1 };

			for (size_t i = 0; i <= p2.degree(); i++)
			{
				poly.add_term(i, p2.at(i));
			}

			return poly;
		}

		// 1.j: A star operator to return a polynomial equal to a product of two polynomials.
		friend polynomial operator*(const polynomial<T>& p1, const polynomial<T>& p2) {
			polynomial<T> result{};

			for (size_t i = 0; i <= p1.degree(); i++)
			{
				for (size_t j = 0; j <= p2.degree(); j++)
				{
					auto power = i + j;
					auto coefficient = p1.poly.at(i) * p2.poly.at(j);
					result.add_term(power, coefficient);
				}
			}

			return result;
		}
		

		friend bool operator==(const polynomial<T>& a, const polynomial<T>& b) {
			if (a.degree() != b.degree()) {
				return false;
			}

			for (size_t i = 0; i <= a.degree(); i++)
			{
				if (a.poly.at(i) != b.poly.at(i)) {
					return false;
				}
			}

			return true;
		}

		friend bool operator!=(const polynomial<T>& a, const polynomial<T>& b) {
			return !(a == b);
		}

		int degree() const;

		T at(std::size_t) const;
	};

	template<typename T>
	std::ostream& operator<<(std::ostream& os, const polynomial<T>& p);
}

#endif