/*
Emil Stensig Jensen
esje11@student.aau.dk
studynumber: 2011 2745
*/

The project has been written in Microsoft Visual Studio Professional 2013 Update 4, and compiled with default settings. This means parts of C++14 has been available, as some parts are not implemented.

A polynomial can be created as polynomial<T>{a, b, c}, where a, b, c are coefficients at the degree of their index, i.e. a+bx+cx^2. Examples of use can be seen in the file main.cpp, as well as expected output.